from FpsoAnalysis import FpsoAnalysis


fpso = FpsoAnalysis()
filepath = '20200415 O&G Equipment Data .xlsx'
data = fpso.load_dataset(filepath)

num_failures, new_data = fpso.calc_cycles_failures(data)
print('1- ' + str(num_failures) + ' equipment failures across ' + str(len(data)) + ' cycles.')

fail_data = fpso.calc_parameters_distribution(new_data)
print("As Figure shows(dist.png and distc.png), the equipment failures are approximately") 
print("normally distributed, but a shift can be identified between Normal")
print("cycles and Failure cycles")

fpso.separate_into_groups(fail_data)
print('2,3 - Applying kmeans cluster and PCA to categorize in groups')
print('PCA perform a linear transformation to represent multidimensional data in n-dimensional.') 
print('The results(pca.png and kmeans.png) show that by reducing the problem to two/three dimensions,')
print('we can represent the equipment failures in a plot and') 
print('suggest some separation into 3 groups. We can then use kmeans to cluster those datapoints into 3 groups')

print('In order to detect equipment failures before they happen,') 
print("we'll train a model that predicts how many cyles there are until the next equipment failure.")
model, history = fpso.predict_equipment_performance(new_data)
print('4 - Figure training.png shows the training progress of the 1D LSTM model.')
print('Figure predict.png show prediction vs expected #cycle until failure.') 
print('We can see that the model can capture some of the trends of the real data. Future optimization can be done decrease MAE error.')

print('Feature importance in neural networks can be inferred by looking at the weights.')
fpso.features_importance(model)
print('5 - Figure features.png show feature importance. There some features with more importance (positive) to the model prediction results')
