import os
import numpy as np
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, LSTM, Dropout, Flatten
from sklearn.metrics import davies_bouldin_score, mean_squared_error, mean_absolute_error
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler
import xgboost as xgb


class FpsoAnalysis:

    def __init__(self):
        pass

    #Load and remove non-numeric data if necessary
    def load_dataset(self, filepath):
        data = pd.read_excel(filepath)
        print(data.head())

        if data.isnull().values.any():
            data = data.apply(pd.to_numeric, errors='coerce')
            data = data.dropna()

        return data

    #Calculate failures in the cycles. 
    #Considere a new failure if a row is a failure AND the preceding row isn't
    def calc_cycles_failures(self, data):
        failures = data['Fail'].values
        new_failures = np.zeros(failures.shape[0])
        for n in range(failures.shape[0]):
            if failures[n] == True and failures[n-1] == False:
                new_failures[n] = True

        num_failures = np.sum(new_failures)
        data['newFail'] = new_failures
        return num_failures, data

    #Calculate all columns/parameters distributions per cycle/cycle type
    def calc_parameters_distribution(self, data, dist_filename='dist.png', dist_cycle_filename='distc.png'):
        fail_data = data.loc[data['newFail'] == True]
        nonfail_data = data.loc[data['Fail'] == False]

        cols = fail_data.columns[3:-2]
        nplot=1
        plt.figure()
        for col in cols:
            plt.suptitle('Parameters Distributions of Sensor Data for Normal and Failure Cycles')
            plt.subplot(2, np.ceil(len(cols)/2), nplot)
            sns.distplot(fail_data[col], hist=False, label='Failure Cycles')
            sns.distplot(nonfail_data[col], hist=False, label='Normal Cycles')            
            nplot+=1
            plt.ylabel('Probability Density')
        plt.tight_layout()
        plt.savefig(dist_filename)

        nplot=1
        plt.figure()
        cycles=list(data['Cycle'].values)
        for col in cols:
            plt.suptitle('Parameters Distributions of Sensor Data for Cycles')
            plt.subplot(2, np.ceil(len(cols)/2), nplot)
            plt.plot(cycles, data[col], 'o')
            nplot+=1
            plt.ylabel(col)
        plt.tight_layout()
        plt.savefig(dist_cycle_filename)

        return fail_data

    #Use PCA and Kmeans to categorize equipments failure
    def separate_into_groups(self, data, pca_filename='pca.png', gr_filename='kmean.png'):
        val_data = data.iloc[:,1:-1].values.T
        enc = OneHotEncoder(sparse=False)
        scaler = MinMaxScaler()

        pres1 = enc.fit_transform(val_data[0].reshape(-1, 1)).T
        pres2 = enc.fit_transform(val_data[1].reshape(-1, 1)).T
        sensor_params = scaler.fit_transform(val_data[2:])
        enc_data = np.concatenate((pres1,pres2,sensor_params)).T

        pca = PCA(n_components=3)
        transf = pca.fit_transform(enc_data).T
        print('principal components: ', pca.explained_variance_ratio_)

        kmeans = KMeans(n_clusters=3, random_state=0).fit_predict(enc_data)

        plt.figure()
        plt.title('PCA on encoded data')
        plt.scatter(transf[0],transf[1])
        plt.ylabel('2nd PCA Dimension')
        plt.xlabel('1st PCA Dimension')
        plt.savefig(pca_filename)

        plt.figure()
        plt.title('Kmeans applied to PCA results')
        c_map = plt.get_cmap('magma',3)
        plt.scatter(transf[0],transf[1],c=kmeans,cmap=c_map)
        plt.ylabel('2nd PCA Dimension')
        plt.xlabel('1st PCA Dimension')
        plt.colorbar(ticks=np.arange(np.min(kmeans),np.max(kmeans)+1))
        plt.savefig(gr_filename)

    def batch_generator(self,X,Y,window_size,overlap=0,cut='top'):
    
        ws = window_size    
        jmp = ws-overlap
        nw = int(np.floor(X.shape[0]/jmp))
        nchan = X.shape[1]
        
        if cut == 'top':
            w0 = X.shape[0] - (nw*jmp)
        elif cut == 'bot':
            w0 = 0
            
        outx = np.zeros((nw,ws,nchan))
        outy = np.zeros((nw,ws))
        
        for n in range(nw):
            current_row = n*jmp
            end_row = current_row+ws
            outx[n] = X[current_row:end_row]
            outy[n] = Y[current_row:end_row]

        return outx,outy

    #Predict number of cycles before failure
    def predict_equipment_performance(self, data, training_filename='training.png', predict_filename='predict.png'):
        fail_cycle = data['Fail'].values
        since_fail = np.zeros(fail_cycle.shape[0])

        since_last=1
        for n,var in enumerate(fail_cycle):        
            if fail_cycle[n] == 1:
                since_last=0
            elif fail_cycle[n] == 0 and fail_cycle[n-1] == 1:
                since_last=1
            elif fail_cycle[n] == 0 and fail_cycle[n-1] == 0:
                since_last=since_last+1
            since_fail[n] = since_last

        idx_fails = np.where(since_fail==0)[0]
        first_jump = idx_fails[0]+1
        until_fail=[list(range(first_jump))[::-1]]
        for i in range(1,idx_fails.shape[0]):
            jump = idx_fails[i]-idx_fails[i-1]
            if jump == 1:
                until_fail.append([0])
            else:
                until_fail.append(list(range(jump))[::-1])

        until_fail = [item for iter in until_fail for item in iter]
           
        data['tofail'] = until_fail
        labels = until_fail
        data = data.loc[data['Fail'] == False]
        data = data.drop(labels=['Cycle','Fail','newFail','tofail'],axis=1)

        enc = OneHotEncoder(sparse=False)
        scaler = MinMaxScaler()

        val_data = data.values.T
        pres1 = enc.fit_transform(val_data[0].reshape(-1, 1)).T
        pres2 = enc.fit_transform(val_data[1].reshape(-1, 1)).T
        sensor_params = scaler.fit_transform(val_data[2:])
        enc_data = np.concatenate((pres1,pres2,sensor_params)).T

        train_size = int(enc_data.shape[0] * 0.6)
        test_size = enc_data.shape[0] - train_size

        X_train, X_test = enc_data[0:train_size,:], enc_data[train_size:enc_data.shape[0],:]
        y_train, y_test = labels[0:train_size], labels[train_size:len(labels)]

        window_size=100
        overlap=0
        X_train, y_train = self.batch_generator(X_train,y_train,window_size,overlap)
        X_test, y_test = self.batch_generator(X_test,y_test,window_size,overlap)

        ks=20
        nf=6
        n_conv=3
        #Define the model
        #https://machinelearningmastery.com/reshape-input-data-long-short-term-memory-networks-keras/
        model = Sequential()
        model.add(LSTM(100,return_sequences=True))
        model.add(LSTM(100,return_sequences=True))
        model.add(LSTM(100,return_sequences=True))
        model.add(Dropout(0.5))
        model.add(Flatten())
        model.add(Dense(window_size,activation='linear'))

        model.compile(optimizer='Adam', loss="mae") #adam, adadelta

        history = model.fit(X_train, y_train, epochs=500, batch_size=32, validation_data=(X_test,y_test),shuffle=False,verbose=0)

        #Objective: reg:logistic, 'reg:linear', reg:squarederror
        #xg_reg = xgb.XGBRegressor(learning_rate = 0.1, max_depth = 40, n_estimators = 50)

        #xg_reg.fit(X_train,y_train)

        #pred = xg_reg.predict(X_test)

        plt.figure()
        plt.plot(history.history['loss'],label='Train')
        plt.plot(history.history['val_loss'],label='Validation')
        plt.ylabel('MAE Loss')
        plt.xlabel('Training Epochs')
        plt.title('Loss from training progress')
        plt.legend()
        plt.savefig(training_filename)

        pred = model.predict(X_test).ravel()

        plt.figure()
        plt.title('Prediction results')
        plt.plot(y_test.ravel(),label='Real data')
        plt.plot(pred,label='Predictions')
        plt.xlabel('Cycles')
        plt.ylabel('Cycles until Failure')
        plt.savefig(predict_filename)

        return model, history

    def features_importance(self, model, features_filename='features.png'):
        weights = model.layers[0].get_weights()[0]
        meanw=[]
        for feat in weights:
            meanw.append(np.mean(feat))

        plt.figure()
        plt.bar(x=list(range(len(meanw))),height=meanw)
        plt.title('Feature weights')
        plt.ylabel('Weights')
        plt.xlabel('Features')
        plt.savefig(features_filename)



