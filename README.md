# README #

FPSO Equipment Failure Analysis

## Installation ##

- Python 3+
- Create Virtual Env: conda create --name fpso_failure_analysis python==3.7.6
- pip install -r requirements.txt

## Content ##

- FpsoAnalysis.py: Class with all variables and methods to answer the 5 questions
- fpso_analysis.py: Script that use de above class to generate the answers with prints and imagens

## Results ##

![dist](dist.png)

![distc](distc.png)

![pca](pca.png)

![kmean](kmean.png)

![training](training.png)

![predict](predict.png)

![features](features.png)
